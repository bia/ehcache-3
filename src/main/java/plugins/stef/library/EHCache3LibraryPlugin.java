package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the EHCache 3 engine / library.
 * 
 * @author Stephane Dallongeville
 */
public class EHCache3LibraryPlugin extends Plugin implements PluginLibrary
{
    //
}
